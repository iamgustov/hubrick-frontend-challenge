const { Rule } = require('../../../src/engine/rule')
const CORRECT_DATA = JSON.parse(require('../../../data/correct.json'))

describe('Engine', () => {
    describe('Rule', () => {
        describe('Rule', () => {
            const ruleObject = CORRECT_DATA[0]

            it('should be able to create a new rule with props', () => {
                const rule = new Rule(ruleObject)
                expect(rule).not.toBe(null)
                expect(rule.id).toBe(ruleObject.id)
            })

            it('should be able to get if the rule is valid or not', () => {
                const invalidRule = new Rule({})
                const validRule = new Rule(ruleObject)
                expect(invalidRule.isValid()).toBe(false)
                expect(validRule.isValid()).toBe(true)
            })

            it('should be able to eval', () => {
                const rule = new Rule(ruleObject)
                expect(rule.execute).toThrow()
            })
        })
    })
})

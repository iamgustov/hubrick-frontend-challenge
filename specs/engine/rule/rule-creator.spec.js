const { RuleCreator } = require('../../../src/engine/rule')
const CORRECT_DATA = JSON.parse(require('../../../data/correct.json'))
const INCORRECT_DATA = JSON.parse(require('../../../data/incorrect.json'))

describe('Engine', () => {
    describe('Rule', () => {
        describe('RuleCreator', () => {
            const correctRules = CORRECT_DATA.map(i => RuleCreator.create(i))
            
            it('should be able to create rules from correct data', () => {
                expect(correctRules.length).toBe(CORRECT_DATA.length)
            })

            it('should be able to create valid rules from correct data', () => {
                expect(correctRules.map(r => r.isValid())).toEqual(CORRECT_DATA.map(_ => true))
            })

            it('should not throw an error if it took the incorrect data', () => {
                expect(() => { CORRECT_DATA.map(i => RuleCreator.create(i)) }).not.toThrow()
            })

            it('should be able to throw an error if it took the incorrect data', () => {
                expect(() => { INCORRECT_DATA.map(i => RuleCreator.create(i)) }).toThrow()
            })
        })
    })
})

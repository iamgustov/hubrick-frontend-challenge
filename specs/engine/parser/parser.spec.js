const { Parser } = require('../../../src/engine/parser/index')
const CORRECT_DATA = JSON.parse(require('../../../data/correct.json'))
const INCORRECT_DATA = JSON.parse(require('../../../data/incorrect.json'))

describe('Engine', () => {
    describe('Parser', () => {
        describe('Parser', () => {
            it('should be able to parse the correct functions', () => {
                const method = CORRECT_DATA[0].rule
                expect(() => Parser.parseStringRule(method)).not.toThrow()
                expect(Object.keys(Parser.parseStringRule(method))).toEqual(expect.arrayContaining(['args', 'body']))
            })

            it('should be able to throw if it found the correct function', () => {
                const method = INCORRECT_DATA[0].rule
                expect(() => Parser.parseStringRule(method)).toThrow()
            })
        })
    })
})

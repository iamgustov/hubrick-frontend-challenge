const { FlowExecutor } = require('../../../src/engine/flow')
const { RuleCreator } = require('../../../src/engine/rule')

const DATA = JSON.parse(require('../../../data/correct.json'))

describe('Engine', () => {
    describe('Flow', () => {
        describe('FlowExecutor', () => {
            const rules = DATA.map(i => RuleCreator.create(i))

            it('should be able to create a new object based on rules passed', () => {
                const flowExecutor = new FlowExecutor(rules)
                expect(flowExecutor).not.toBe(null)
            })

            it('should be able to assing internal map with the same rules values', () => {
                const flowExecutor = new FlowExecutor(rules)
                expect(Object.keys(flowExecutor.map).length).toBe(rules.length)
            })
            
            it('should be able to assing internal map with the referred rules values', () => {
                const flowExecutor = new FlowExecutor(rules)
                const key = rules[0].id
                expect(flowExecutor.map[key].id).toBe(key)
            })

            it('should be able to execute rules without callback', () => {
                const flowExecutor = new FlowExecutor(rules)
                const key = rules[0].id
                expect(() => flowExecutor.execute(key, { color: "green" })).not.toThrow()
            })
        })
    })
})

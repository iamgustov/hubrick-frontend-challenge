# Hubrick Frontend Challenge

To run the project make sure you are on node v 9.5.0 or higher. The task is working only over node with ES6 syntax.
To start the project simply to:

* `cd YOUR_FOLDER`
* `yarn # to install the modules`
* `yarn run test # to run specs`
* `yarn start # to see the result`

Also you can find test correct and incorrect data in `data/*` folder.
If you have any question please ping me. Thanks for the review.
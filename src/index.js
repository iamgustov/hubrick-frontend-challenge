const { Engine, Printer } = require('./engine')
const DATA = JSON.parse(require('../data/correct.json'))

Printer.print('\n************* STARTING THE ENGINE... *************\n')

// Printer.print(DATA)
const object = { color: 'red' }
Engine.start(DATA, object, ({ rule, result, nextId }) => {
    const method = Printer[result ? 'g' : 'r']
    const [printPrefix, printResult, printObject, printPostfix] = [
        method(`Rule #${rule.id}`),
        method(result ? 'passed' : 'failed'),
        Printer.y(JSON.stringify(object)),
        !!nextId ? `Next id: ${Printer.y(nextId)}` : Printer.g('End'),
    ]

    Printer.print(`${printPrefix}: ${printResult} during ${printObject} evaluation... ${printPostfix}...`)
})

Printer.print('\n************* ENGINE HAS COMPLETED THE EVALUATION... *************\n')

const { RuleCreator } = require('../rule')
const FlowExecutor = require('./flow-executor')

const rulesFromData = (data) => data.map(i => RuleCreator.create(i))

class Flow {
    constructor(data = []) {
        this.rules = rulesFromData(data)
    }

    execute(object, callback) {
        // let's just assume the first rule is the initial one...
        new FlowExecutor(this.rules).execute(this.rules[0].id, object, callback)
    }
}

module.exports = Flow
// let's just assume we won't get to infinite loop... othrwise we need to add calls counter, etc
class FlowExecutor {
    constructor(rules) {
        this.map = {}
        rules.forEach((rule) => {
            this.map[rule.id.toString()] = rule
        })
    }

    execute(id, object, callback) {
        const rule = this.map[id.toString()]
        const result = rule.execute(object)
        const nextId = result ? rule.trueId : rule.falseId
        
        if (callback) callback({ rule, result, nextId })
        if (!nextId) return
        
        this.execute(nextId, object, callback)
    }
}

module.exports = FlowExecutor
const { Flow } = require('./flow')

class Engine {
    static start(data, object, callback) {
        new Flow(data).execute(object, callback)
    }
}

module.exports = Engine
class ParserError {
    constructor(message) {
        this.name = this.constructor.name
        this.message = message
    }
}

ParserError.prototype = Object.create(Error.prototype)
ParserError.prototype.constructor = ParserError

module.exports = ParserError
const ParserError = require('./parser-error')

const FUNCTION_REGEXP = /(function\s?)([^\.])([\w|,|\s|-|_|\$]*)(.+?\{)([^\.][\s|\S]*(?=\}))/

class Parser {
    static parseStringRule(string) {
        const match = string.match(FUNCTION_REGEXP)
        if (!match) throw new ParserError('Parser: failed during method parsing')
        return {
            args: match[3].split(',').map(arg => arg.trim()),
            body: match[5]
        }
    }
}

module.exports = Parser
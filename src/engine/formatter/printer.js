const COLORS = {
    red: "\x1b[31m",
    green: "\x1b[32m",
    yellow: "\x1b[33m",
    terminate: "\x1b[0m"
}

class Printer {
    static r(string) {
        return `${COLORS.red}${string}${COLORS.terminate}`
    }

    static g(string) {
        return `${COLORS.green}${string}${COLORS.terminate}`
    }

    static y(string) {
        return `${COLORS.yellow}${string}${COLORS.terminate}`
    }

    static print(string) {
        console.log(string)
    }
}

module.exports = Printer
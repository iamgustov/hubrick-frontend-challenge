const Rule = require('./rule')
const { Parser } = require('../parser')

class RuleCreator {
    static create({ id, rule, trueId, falseId }) {
        const { args, body } = Parser.parseStringRule(rule)
        const ruleMethod = new Function(...args, body)
        return new Rule({ id, rule: ruleMethod, trueId, falseId })
    }
}

module.exports = RuleCreator
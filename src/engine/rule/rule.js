const REQUIRED_FIELDS = ['id', 'rule']

class Rule {
    constructor({ id, trueId, falseId, rule }) {
        this.id = id
        this.trueId = trueId
        this.falseId = falseId
        this.rule = rule
    }

    isValid() {
        let valid = true
        REQUIRED_FIELDS.forEach((field) => {
            if (!this[field]) {
                valid = false
            }
        })
        return valid
    }

    execute(object) {
        return !!this.rule(object) // let's convet it to Bool in case we get some other value type differs from Bool
    }
}

module.exports = Rule